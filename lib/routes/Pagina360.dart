import 'package:flutter/material.dart';
import 'package:casa_bunicii/widgets/TurVirtualWebView.dart';
import 'package:casa_bunicii/widgets/BottomNavigationBar.dart';

class Pagina360 extends StatelessWidget {
  final String titlu;
  final String urlTurVirtual;
  Pagina360({this.titlu, this.urlTurVirtual});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
        title: Text(
          '$titlu',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigation(),
      body: TurVirtual(
        url: urlTurVirtual,
      ),
    );
  }
}
