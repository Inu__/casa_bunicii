import 'package:flutter/material.dart';
import 'package:casa_bunicii/routes/Pagina360.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Casa Bunicii',
      theme: ThemeData.dark(),
      home: Pagina360(
        titlu: 'Casa Pizdoasa',
        urlTurVirtual: 'http://google.com',
      ),
    );
  }
}
