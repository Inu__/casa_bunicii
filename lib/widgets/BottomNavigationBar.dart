import 'package:flutter/material.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FlatButton(
              onPressed: () {
                print('pressed message');
              },
              child: Icon(
                Icons.message,
                color: Colors.white,
                size: 50,
              ),
            ),
            FlatButton(
              onPressed: () {
                print('pressed phone');
              },
              child: Icon(
                Icons.phone,
                size: 50,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
      color: Colors.blueGrey[900],
    );
  }
}
